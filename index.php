<!DOCTYPE html>
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include("header.php") ?>
 

<main role="main">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col col-lg-2">
				</div>
					<div class="col-md-auto">    
						<div class="row">
							<div class="col-12">
								<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
									</ol>
									<div class="carousel-inner">
											<div class="carousel-item active">
												<img class="d-block w-100" src="/img/mp_10.jpg" width="400" height="600" alt="Первый слайд">
												<div class="carousel-caption d-none d-md-block">
													<h1 class="jumbotron-heading">Домены .INK, .WIKI и .DESIGN от 799 рублей</h1>
													<p class="lead text-muted">Веб-адреса для творческих и образовательных проектов по сниженным ценам</p>
												</div>
											</div>
										<div class="carousel-item">
											<img class="d-block w-100" src="/img/mp_7.jpg" width="400" height="600" alt="Второй слайд">
										</div>
									</div>
								<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Предыдущий</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Следующий</span>
								</a>
								</div>
							</div>
						</div>
					</div>
				<div class="col col-lg-2">
				</div>
			</div>
		</div>
	  <br>

		<div class="container">
		  <div class="row"> 
				<div class="card-group">
				  <div class="col-sm"> 
						<div class="card">
							<img src="/img/1.jpg" class="card-img-top" alt="Хостинг">
							<div class="card-body">
								<h5 class="card-title">Хостинг и серверы</h5>
								<p class="card-text">Надёжный классический и VIP хостинг, VPS с SSD+HDD и SSD носителями, Dedicated. Домены в подарок.</p>
							</div>
						</div>
					</div>
					<div class="col-sm"> 
						<div class="card">
							<img src="/img/2.jpg" class="card-img-top" alt="SSL-сертификаты">
							<div class="card-body">
								<h5 class="card-title">SSL-сертификаты</h5>
								<p class="card-text">Гарантия статуса сайта и безопасности передаваемых данных. Незаменимо для e-commerce.</p>
							</div>
						</div>
					</div>
					<div class="col-sm"> 
						<div class="card">
							<img src="/img/3.jpg" class="card-img-top" alt="Автоматическое SEO-продвижение">
							<div class="card-body">
								<h5 class="card-title">Автоматическое SEO-продвижение<h5>
								<p class="card-text">Управление автоматизированным продвижением в поисковых системах.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<br>

		<div class="container">
			<div class="row">
				<div class="col">
					<h3> Новости</h3>
						<p class="date">13 июня 2019 г.</p>
						<p>Скидка 50% на услугу «Администрирование сервера»</p>
						<p>Сэкономьте до 42% на SSL-сертификате до 20 июня</p>
						<p class="date">11 июня 2019 г.</p>
						<p>Только до 14 июня: домены .RU, .РФ, .SU, .МОСКВА и .MOSCOW от 177 рублей</p>
						<p class="date">10 июня 2019 г.</p>
						<p>Бесплатное скрытие персональных данных при регистрации международных доменов</p>
				</div>
				<div class="col">
					<h3>Мероприятия</h3>
						<p class="date">19 июня, Москва</p>
						<p>Скидка на участие в конференции Social Media Moscow</p>
						<p class="date">21–22 июня, Москва</p>
						<p>DevConf — конференция для профессиональных веб-разработчиков</p>
				</div>
			</div>
		</div>
	<br>  



		<div class="container">
			<h2>Почему мы? Это просто</h2>
				<div class="row">
					<div class="row">
						<div class="col">
										№1
									<p>регистратор и хостинг-провайдер в России</p>
						</div>
						<div class="col">
										34 млн
									<p>оказанных услуг</p>
						</div>
						<div class="col">
										3 млн
									<p>доменов на обслуживании</p>
						</div>
						<div class="col">
										1.9 млн

									<p>более 1.9 млн клиентов</p>
						</div>
						<div class="col">
										24/7
									<p>профессиональная поддержка</p>
						</div>
					</div>
				</div>
	<br>

		<div class="container">
			<blockquote>
				<h2>Отзывы клиентов</h2>
					<p>Доброго дня! Меня зовут Виталий, я веб-мастер.
						Стаж работы 6 лет. У меня около 30 активных сайтов на разных хостингах.
						REG.RU самый лучший из всех, которые я знаю. По всем показателям. Это проверено временем.
						Поэтому все новые проекты я делаю уже тут. А также участвую в партнёрской программе.
						От души благодарю всю команду за Вашу замечательную, компетентную и достойную работу!
					</p>
			</blockquote>		
		</div>
	 
 </main>
 
<?php include ("footer.php") ?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="/js/script.js"></script>
</body>
</html>
