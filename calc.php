<!DOCTYPE html>
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include("header.php") ?>

<main>

<div class="container">
<h1>Хостинг</h1>
<div class="card-group">
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Host-0</h5>
      <p class="card-text">
			<p>12 ГБ SSD</p>
			<p>7 сайтов и ∞ БД</p>
			<p>∞ псевдонимов</p>
			<p>∞ трафик</p>
			<p>ISPmanager</p>
			<p>Plesk</p>
			<p>CPanel</p>
			<p>от 129 ₽/мес.</p>
		</p>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Host-1</h5>
      <p class="card-text">
	  <p>17 ГБ SSD</p>
			<p>15 сайтов и ∞ БД</p>
			<p>∞ псевдонимов</p>
			<p>∞ трафик</p>
			<p>ISPmanager</p>
			<p>Plesk</p>
			<p>CPanel</p>
			<p>от 210 ₽/мес.</p></p>
          </div>
  </div>
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Host-3</h5>
      <p class="card-text">
	  <p>27 ГБ SSD</p>
			<p>30 сайтов и ∞ БД</p>
			<p>∞ псевдонимов</p>
			<p>∞ трафик</p>
			<p>ISPmanager</p>
			<p>Plesk</p>
			<p>CPanel</p>
			<p>от 324 ₽/мес.</p>
			</p>
          </div>
  </div>
</div>
</div>
</main>






<?php include ("footer.php") ?>